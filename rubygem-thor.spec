%global gem_name thor

Name:           rubygem-%{gem_name}
Version:        1.2.1
Release:        1
Summary:        Thor is a toolkit for building powerful command-line interfaces
License:        MIT
URL:            http://whatisthor.com/
Source0:        https://rubygems.org/gems/%{gem_name}-%{version}.gem
Source1:        %{gem_name}-%{version}-spec.txz

Patch0:         rubygem-thor-1.2.1-Fix-expectations-for-ruby-3-treatment-of-hash-arg.patch
Patch1:         rubygem-thor-1.2.1-Fix-rspec-mocks-3.11.0-compatibility.patch
Patch2:         rubygem-thor-1.2.1-did_you_mean-ruby32.patch

Requires:       rubygem(io-console)
BuildRequires:  ruby(release)
BuildRequires:  rubygems-devel
BuildRequires:  ruby
BuildRequires:  rubygem(io-console)
BuildRequires:  rubygem(rake)
BuildRequires:  rubygem(rspec)
BuildRequires:  rubygem(webmock)
BuildRequires:  rubygem(did_you_mean)
BuildRequires:  git
BuildArch:      noarch

%description
Thor is a toolkit for building powerful command-line interfaces.

%package 	doc
Summary: 	Documentation for %{name}
Requires: 	%{name} = %{version}-%{release}
BuildArch: 	noarch

%description    doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b1

%patch2 -p1
pushd %{_builddir}
%patch0 -p1
%patch1 -p1
popd

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

find %{buildroot}%{gem_instdir}/bin -type f | \
  xargs -n 1 sed -i -e 's"^#!/usr/bin/env ruby"#!/usr/bin/ruby"'

%check
pushd .%{gem_instdir}
cp -r %{_builddir}/spec .
sed -i '/simplecov/,/end/ s/^/#/' spec/helper.rb
LC_ALL=C.UTF-8 rspec -rreadline spec
popd

%files
%dir %{gem_instdir}
%{_bindir}/thor
%license %{gem_instdir}/LICENSE.md
%{gem_instdir}/bin
%{gem_libdir}
%exclude %{gem_instdir}/.document
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CONTRIBUTING.md
%doc %{gem_instdir}/README.md
%{gem_instdir}/thor.gemspec

%changelog
* Thu Aug 10 2023 wubijie <wubijie@kylinos.cn> - 1.2.1-1
- Upgrade to version 1.2.1

* Tue Jan 17 2023 yaoxin <yaoxin30@h-partners.com> - 1.1.0-4
- Fix build failed due to ruby update to 3.1.3

* Wed Jun 29 2022 liyanan <liyanan32@h-partners.com> - 1.1.0-3
- Fix build failed with rubygem-rspec-mocks

* Wed Mar 30 2022 ouyangminxiang <ouyangminxiang@kylinsec.com.cn> - 1.1.0-2
- add BuildRequires: rubygem-bigdecimal rubygem-io-console rubygem-openssl rubygem-psych 
  to resolve koji build error during check

* Fri Mar 04 2022 wangkerong <wangkerong@h-partners.com> - 1.1.0-1
- Upgrade to 1.1.0

* Sat Sep 5 2020 liyanan <liyanan32@huawei.com> - 0.20.3-2
- fix build fail

* Mon Aug 10 2020 yanan li <liyanan032@huawei.com> - 0.20.3-1
- Package init

